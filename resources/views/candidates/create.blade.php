@extends('layouts.app')

@section('content')
    <body>
        <h1>create candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
            @csrf
            <div>
                <label for = "name">Candidate name</label>
                <input type = "text" name = "name">
            </div>
            <div>
                <label for = "name">Candidate email</label>
                <input type = "text" name = "email">
            </div>
            <div>
                <input type = "submit" name = "submit" value = "Create candidate">
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
             @endforeach
            </ul>
            </div>
        @endif
        </form>
    </body>
@endsection


////