<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    protected $fillable = [
        'name'
    ];
    public function candidates(){
        return $this->hasMany('App\Candidate');
    }
    public static function next($status_id){
       $nextstages =  DB::table('nextstages')->where('from',$status_id)->pluck('to');
       return self::find($nextstages)->all();
    }

    public static function allowed($to,$from){
        if(($from =='1' AND $to =='2') OR ($from =='1' AND $to =='3')){
        return TRUE;
        }
        elseif(($from =='3' AND $to =='4') OR ($from =='3' AND $to =='5')){
        return TRUE;}
        else{
        return FALSE;}

 }

}
